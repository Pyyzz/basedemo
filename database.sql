create database demo;

use demo;

create table information (
id int AUTO_INCREMENT,
name varchar(100) NOT NULL,
phone varchar(100) NOT NULL,
address varchar(100) NOT NULL,
Primary key(id)
);

insert into information(name, phone, address) values ('Phong', '01234', 'HaiPhongCity');
insert into information(name, phone, address) values ('Phong1', '012345', 'HaiPhongCity1');
insert into information(name, phone, address) values ('Phong2', '0123456', 'HaiPhongCity2');

drop table information;
drop database demo

