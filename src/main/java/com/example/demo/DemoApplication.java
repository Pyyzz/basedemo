package com.example.demo;

import com.example.demo.repository.InformationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@RequiredArgsConstructor
public class DemoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        InformationRepository informationRepository = context.getBean(InformationRepository.class);

        informationRepository.findAll().forEach(System.out::println);
    }

}
