package com.example.demo.dao;


import java.util.Optional;

public interface GeneralDAO<T> {
    Iterable<T> findAll();
    Optional<T> findById(int id);
    T save(T t);
    void remove(int id);
}
