package com.example.demo.dao.Dao;

import com.example.demo.dao.GeneralDAO;
import com.example.demo.entity.Information;

public interface InformationDAO extends GeneralDAO<Information> {
}
