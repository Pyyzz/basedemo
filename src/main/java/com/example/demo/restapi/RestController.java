package com.example.demo.restapi;

import com.example.demo.dao.Dao.InformationDAO;
import com.example.demo.entity.Information;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@CrossOrigin("*")
@RequestMapping("/information")
public class RestController {
    @Autowired
    private InformationDAO informationDAO;

    // Don't use @RequestBody, use @ModelAttribute
    @PostMapping()
    public ResponseEntity<Information> createNewCategory(@ModelAttribute Information information) {
        return new ResponseEntity<>(informationDAO.save(information), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<Iterable<Information>> getAllCategory() {
        return new ResponseEntity<>(informationDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Information> getCategory(@PathVariable int id) {
        Optional<Information> informationOptional = informationDAO.findById(id);
        return informationOptional.map(information -> new ResponseEntity<>(information, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Information> updateCategory(@PathVariable int id, @ModelAttribute Information information) {
        Optional<Information> informationOptional = informationDAO.findById(id);
        return informationOptional.map(information1 -> {
            information.setId(information1.getId());
            return new ResponseEntity<>(informationDAO.save(information), HttpStatus.OK);
        }).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Information> deleteCategory(@PathVariable int id) {
        Optional<Information> informationOptional = informationDAO.findById(id);
        return informationOptional.map(information -> {
            informationDAO.remove(id);
            return new ResponseEntity<>(information, HttpStatus.OK);
        }).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
