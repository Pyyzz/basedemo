package com.example.demo.daoimpl;

import com.example.demo.dao.Dao.InformationDAO;
import com.example.demo.entity.Information;
import com.example.demo.repository.InformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InformationDAOImpl implements InformationDAO {
    @Autowired
    private InformationRepository informationRepository;

    @Override
    public Iterable<Information> findAll() {
        return informationRepository.findAll();
    }

    @Override
    public Optional<Information> findById(int id) {
        return informationRepository.findById(id);
    }

    @Override
    public Information save(Information information) {
        return informationRepository.save(information);
    }

    @Override
    public void remove(int id) {
       informationRepository.deleteById(id);
    }
}
